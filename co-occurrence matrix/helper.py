import codecs
import os
import pickle
import time
from itsdangerous import json
import contextchains
import vocab
import glove_preprocess


def current_milli_time():
    return round(time.time() * 1000)


# remove element with index i from list
def remove(i, this_list: []):
    new_list = []
    for element in this_list:
        if element[0] != i:
            new_list.append(element)
    return new_list


def write_count_to_file(counter, filename):
    with open(filename, 'w') as file123:
        file123.write(str(counter))


# mit umlauten
def write_dict_to_file(dictionary, filename):
    with codecs.open(filename, 'w', "utf-8") as convert_file:
        convert_file.write(json.dumps(dictionary, ensure_ascii=False))


# Für Glove
def get_glove_parallel_parameters():
    token_len = get_token_len_glove()
    slicesize = int((480000000 / 4) // token_len)
    num_of_slices = token_len // slicesize
    if (num_of_slices * slicesize) < token_len:
        num_of_slices = num_of_slices + 1
    number_list = []
    index_slices = []
    xab = 0
    for i in range(num_of_slices):
        number_list.append(i)
    check_count = 0
    xab = 0
    # Ein Index Slice ist eine Liste von Indexen im Wortschatz als [0, ... ,1999] bis [58000, ... ,59999]
    for i in range(num_of_slices):
        index_slice = []
        for j in range(slicesize):
            index_slice.append((i * slicesize) + j)
            check_count = check_count + 1
            if check_count == token_len:
                break
        index_slices.append(index_slice)
    xab = 0
    token_len_list = [token_len] * num_of_slices
    file_t = open("/home/felix/PycharmProjects/pythonProject/glove_num_of_slices.txt", "w")
    file_t.write("Anzahl an Slices: {}".format(num_of_slices))
    file_t.close()
    return index_slices, number_list, token_len_list


# Für Dependency Glove
# sollte 30 slices bestimmen, damit der Co-Occurrence-Count funktioniert, bzw. das Speichern des Counts.
def get_parallel_parameters():
    token_len = get_token_len()
    slicesize = int((480000000 / 4) // token_len)
    num_of_slices = token_len // slicesize
    if (num_of_slices * slicesize) < token_len:
        num_of_slices = num_of_slices + 1
    number_list = []
    index_slices = []
    for i in range(num_of_slices):
        number_list.append(i)
    check_count = 0
    for i in range(num_of_slices):
        index_slice = []
        for j in range(slicesize):
            index_slice.append((i * slicesize) + j)
            check_count = check_count + 1
            if check_count == token_len:
                break
        index_slices.append(index_slice)
    token_len_list = [token_len] * num_of_slices
    file_t = open("/home/felix/PycharmProjects/pythonProject/num_of_slices.txt", "w")
    file_t.write("Anzahl an Slices: {}".format(num_of_slices))
    file_t.close()
    return index_slices, number_list, token_len_list


# sollten 60.000 sein
def get_token_len():
    file = open("/home/felix/PycharmProjects/pythonProject/c_vocab/token_len.txt", "r")
    lines = file.readlines()
    a: int = int(lines[0])
    lines.clear()
    file.close()
    return a


def get_token_len_glove():
    file = open("/home/felix/PycharmProjects/pythonProject/c_glove_vocab/token_len.txt", "r")
    lines = file.readlines()
    a: int = int(lines[0])
    lines.clear()
    file.close()
    return a


def set_vocab_and_contextwindows(directorypath, vocabulary):
    # iteriert über alle files im angegebenen Pfad
    file_list = os.listdir(directorypath)
    index = 1
    total_corpus_len = 0
    for filename in file_list:
        print(index)
        filepath = directorypath + "/" + filename
        file = open(filepath)
        # generiert Liste von Sätzen, Ein Satz ist eine Liste von Wörtern
        list_of_sentences = glove_preprocess.get_list_of_sentences(file)
        file.close()
        # zählt die Häufigkeit der Vokabeln und die Größe des Korpus
        for sentence in list_of_sentences:
            for word in sentence:
                vocabulary.add_token_count(word)
                total_corpus_len = total_corpus_len + 1
        sentence_directory = "/home/felix/PycharmProjects/pythonProject/c_glove_sentences/"
        # speichert die aus der CONLL File extrahierten Sätze
        sentence_filename = sentence_directory + "sentence{}.pkl".format(index)
        open_file = open(sentence_filename, "wb")
        pickle.dump(list_of_sentences, open_file)
        index = index + 1
        list_of_sentences.clear()
        open_file.close()
    # speichert die Korpus Größe
    open_file = open("/home/felix/PycharmProjects/pythonProject/c_glove_vocab/corpus_len.txt", "w")
    open_file.write("{}\n".format(total_corpus_len))
    open_file.close()


def set_vocab_and_context_chains(directorypath, vocabulary):
    # iteriert über alle files im angegebenen Pfad
    file_list = os.listdir(directorypath)
    index = 1
    total_corpus_len = 0
    for filename in file_list:
        print(index)
        filepath = directorypath + "/" + filename
        file = open(filepath, "r")
        # erklärung in contextchains.py
        list_of_word_lists_of_file = contextchains.get_list_of_word_lists(file)
        file.close()
        # [index in sentence, word, word stem, relation in the sentence, head of the relation]
        # erklärung in contextchains.py
        context_chain_list, list_of_word_lists_of_file = \
            contextchains.get_all_context_chains(list_of_word_lists_of_file)
        # zählt häufigkeit der Tokens und die Gesamtgröße des Korpus
        for wordlist in list_of_word_lists_of_file:
            for word in wordlist:
                total_corpus_len = total_corpus_len + 1
                vocabulary.add_token_count(word[1])
        # speichert die gefundenen Contextchains in der File in der context_chain_directory
        context_chain_directory = "/home/felix/PycharmProjects/pythonProject/c_contextchainlists/"
        context_chain_filename = context_chain_directory + "contextchain{}.pkl".format(index)
        open_file = open(context_chain_filename, "wb")
        pickle.dump(context_chain_list, open_file)
        index = index + 1
        list_of_word_lists_of_file.clear()
        context_chain_list.clear()
        open_file.close()
    # speichert total corpus len des Dependency Glove Modells
    open_file = open("/home/felix/PycharmProjects/pythonProject/c_vocab/corpus_len.txt", "w")
    open_file.write("{}\n".format(total_corpus_len))
    open_file.close()


# funktioniert im Prinzip wie set_reduced_context_chains
def set_reduced_sentences(directorypath, vocabulary):
    file_list = os.listdir(directorypath)
    index = 1
    reduced_dir_path = "/home/felix/PycharmProjects/pythonProject/c_reduced_sentences/"
    reduced_filename = "list1.txt"
    reduced_file = open(reduced_dir_path + reduced_filename, "a")
    reduced_file.close()
    count = 0
    for filename in file_list:
        count = count + 1
        print("count: " + str(count))
        filepath = directorypath + "/" + filename
        open_file = open(filepath, "rb")
        # loaded_list = liste von Sätzen
        loaded_list = pickle.load(open_file)
        open_file.close()
        reduced_sentences, list_len = glove_preprocess.get_reduced_sentences(loaded_list, vocabulary.token_to_index)
        loaded_list.clear()
        reduced_filename = "list{}.txt".format(index)
        reduced_file = open(reduced_dir_path + reduced_filename)
        reduced_file_len = 0
        for _ in reduced_file:
            reduced_file_len = reduced_file_len + 1
        reduced_file.close()
        maxlen = 200000000
        difference = maxlen - reduced_file_len
        if difference >= list_len:
            reduced_filename = "list{}.txt".format(index)
            reduced_file = open(reduced_dir_path + reduced_filename, "a")
            for sentence in reduced_sentences:
                for word in sentence:
                    reduced_file.write(str(word) + "\n")
            reduced_file.close()
        else:
            reduced_filename = "list{}.txt".format(index)
            reduced_file = open(reduced_dir_path + reduced_filename, "a")
            x = 0
            while x < difference:
                sentence = reduced_sentences.pop()
                for word in sentence:
                    x = x + 1
                    reduced_file.write(str(word) + "\n")
            reduced_file.close()
            index = index + 1
            reduced_filename = "list{}.txt".format(index)
            reduced_file = open(reduced_dir_path + reduced_filename, "a")
            for sentence in reduced_sentences:
                for word in sentence:
                    reduced_file.write(str(word) + "\n")
            reduced_file.close()
        reduced_sentences.clear()


# reduziert die Contextchains auf die Indizes
def set_reduced_context_chains(directorypath, vocabulary):
    # iteriert über die Files im angegebenen Pfad
    file_list = os.listdir(directorypath)
    index = 1
    reduced_dir_path = "/home/felix/PycharmProjects/pythonProject/c_reduced_context_lists/"
    reduced_filename = "list1.txt"
    reduced_file = open(reduced_dir_path + reduced_filename, "a")
    reduced_file.close()
    count = 0
    # die maximale Länge der reduzierten Contextchain Files sind 200.000.000 Einträge um den Ram beim Co-Oc-Count
    # nicht zu überlasten. Es geht darum die Contextchains einer eingelesenen File an der richtigen Stelle zu trennen
    # und eine neue File zu starten mit den restlichen Contextchains, sobald die maximale Anzahl an Einträgen
    # überschritten wurde
    for filename in file_list:
        count = count + 1
        print("count: " + str(count))
        filepath = directorypath + "/" + filename
        open_file = open(filepath, "rb")
        # lädt die Liste von Contextchains aus der File
        loaded_list: list = pickle.load(open_file)
        open_file.close()
        # erstellt die auf den Index der Tokens reduzierten Contextchains
        reduced_context_chain_list, list_len = contextchains.reduce_context_chains(loaded_list, vocabulary.token_to_index)
        loaded_list.clear()
        reduced_filename = "list{}.txt".format(index)
        # im folgenden werden alle Contextchains in eine große Liste bzw mehrere große Listen im .txt Format gespeichert
        reduced_file = open(reduced_dir_path + reduced_filename)
        reduced_file_len = 0
        for _ in reduced_file:
            reduced_file_len = reduced_file_len + 1
        reduced_file.close()
        maxlen = 200000000
        difference = maxlen - reduced_file_len
        if difference >= list_len:
            reduced_filename = "list{}.txt".format(index)
            reduced_file = open(reduced_dir_path + reduced_filename, "a")
            for context_chain in reduced_context_chain_list:
                for context_index in context_chain:
                    reduced_file.write(str(context_index) + "\n")
            reduced_file.close()
        else:
            reduced_filename = "list{}.txt".format(index)
            reduced_file = open(reduced_dir_path + reduced_filename, "a")
            x = 0
            while x < difference:
                context_chain = reduced_context_chain_list.pop()
                for context_index in context_chain:
                    x = x + 1
                    reduced_file.write(str(context_index) + "\n")
            reduced_file.close()
            index = index + 1
            reduced_filename = "list{}.txt".format(index)
            reduced_file = open(reduced_dir_path + reduced_filename, "a")
            for context_chain in reduced_context_chain_list:
                for context_index in context_chain:
                    reduced_file.write(str(context_index) + "\n")
            reduced_file.close()
        reduced_context_chain_list.clear()


# fügt die Zeilenanzahl an den Anfang der File hinzu
def prepend_line_number(directorypath):
    file_list = os.listdir(directorypath)
    i = 1
    for filename in file_list:
        print(i)
        print(filename)
        filepath = directorypath + "/" + filename
        new_file_path = "/home/felix/PycharmProjects/pythonProject/c_reduced_and_prepended_context_lists" + "/{}.txt".format(i)
        file = open(filepath, "r")
        line_number = 0
        for _ in file:
            line_number = line_number + 1
        file_new = open(new_file_path, "w")
        file_new.write(str(line_number) + "\n")
        file.seek(0)
        for line in file:
            file_new.write(line)
        file.close()
        file_new.close()
        i = i + 1


# ist nur eine seperate Funktion, da der Speicherort ein anderer ist
def prepend_glove_line_number(directorypath):
    file_list = os.listdir(directorypath)
    i = 1
    for filename in file_list:
        print(i)
        print(filename)
        filepath = directorypath + "/" + filename
        xab = 0
        new_file_path = "/home/felix/PycharmProjects/pythonProject/c_reduced_and_prepended_sentences" + "/{}.txt".format(i)
        file = open(filepath, "r")
        line_number = 0
        for _ in file:
            line_number = line_number + 1
        file_new = open(new_file_path, "w")
        file_new.write(str(line_number) + "\n")
        file.seek(0)
        xab = 1
        for line in file:
            file_new.write(line)
        file.close()
        file_new.close()
        i = i + 1


def save_vocab_entries(directorypath, vocabulary: vocab.Vocab):
    index_token_fn = directorypath + "index_to_token.pkl"
    token_index_fn = directorypath + "token_to_index.pkl"
    token_stems_fn = directorypath + "token_to_stems.pkl"
    token_len_fn = directorypath + "token_len.txt"
    index_token_file = open(index_token_fn, "wb")
    pickle.dump(vocabulary.index_to_token, index_token_file)
    index_token_file.close()
    token_index_file = open(token_index_fn, "wb")
    pickle.dump(vocabulary.token_to_index, token_index_file)
    token_index_file.close()
    token_stems_file = open(token_stems_fn, "wb")
    pickle.dump(vocabulary.token_to_stems_after_sort, token_stems_file)
    token_stems_file.close()
    token_len_file = open(token_len_fn, "w")
    token_len_file.write(str(len(vocabulary.index_to_token)) + "\n")
    token_len_file.close()


# TODO weiter unten sind keine relevanten Funktionen mehr für den Count bzw. Preprocessing
# die Funktionen wurden nur genutzt um die Corpusfiles vor dem Parsing zu splitten
# spielt keine Rolle für das erstellen des Co-Occurrence-Counts
# ließt alle files im directory path nacheinander ein und splittet sie in je 32 teile, gibt sie aus in outputpath
# nummeriert von counting-1 bis (counting-1) + 32 * len(filelist)
def split_conll_in_n(n):
    outputfilepath = "/home/felix/PycharmProjects/pythonProject/c_conllparts"
    directorypath = "/home/felix/PycharmProjects/pythonProject/c_conllfiles"
    file_list = os.listdir(directorypath)
    counting = 33
    for filename in file_list:
        print(filename)
        inputfilepath = directorypath + "/" + filename
        file = open(inputfilepath, "r")
        filelist = file.readlines()
        spiltlist_len = len(filelist) // n
        sentence_finished = True
        new_file_list = []
        new_file_list_len_count = 0
        filelist_len = len(filelist)
        for i in range(1, n + 1):
            oldj = 0
            for j in range(spiltlist_len):
                if sentence_finished:
                    new_file_list.append(filelist[(i - 1) * spiltlist_len + j])
                    new_file_list_len_count = new_file_list_len_count + 1
                else:
                    if not filelist[(i - 1) * spiltlist_len + j].strip():
                        sentence_finished = True
                        new_file_list.append(filelist[(i - 1) * spiltlist_len + j])
                        new_file_list_len_count = new_file_list_len_count + 1
                        file1 = open(outputfilepath + "/{}.conll".format(counting - 1), "w")
                        print(outputfilepath + "/{}.conll".format(counting - 1))
                        file1.writelines(new_file_list)
                        new_file_list = []
                        file1.close()
                    else:
                        sentence_finished = False
                        new_file_list.append(filelist[(i - 1) * spiltlist_len + j])
                        new_file_list_len_count = new_file_list_len_count + 1
                oldj = j
            if not filelist[(i - 1) * spiltlist_len + oldj].strip():
                sentence_finished = True
            else:
                sentence_finished = False
            if sentence_finished and i != n:
                file1 = open(outputfilepath + "/{}.conll".format(counting), "w")
                print(outputfilepath + "/{}.conll".format(counting))
                file1.writelines(new_file_list)
                new_file_list = []
                file1.close()
            if i == n:
                if new_file_list_len_count < filelist_len:
                    ranger = filelist_len - new_file_list_len_count
                    for t in range(ranger):
                        new_file_list.append(filelist[new_file_list_len_count + t])
                file1 = open(outputfilepath + "/{}.conll".format(counting), "w")
                print(outputfilepath + "/{}.conll".format(counting))
                file1.writelines(new_file_list)
                new_file_list = []
                file1.close()
            counting = counting + 1


# spielt keine Rolle für das Erstellen der Co-Occurrence-Matrix
def create_listed_sentences():
    path = "/home/felix/PycharmProjects/pythonProject/c_rest_of_shuffled_files"
    longlist = []
    shuffle_break = []
    filelist = os.listdir(path)
    for file1 in filelist:
        if len(longlist) < 20000000:
            filepath = path + "/" + file1
            file_o = open(filepath, "r")
            file_list = file_o.readlines()
            for i in range(len(file_list)):
                if len(longlist) < 20000000:
                    longlist.append(file_list[i])
                else:
                    shuffle_break.append(file_list[i])
            file_o.close()
        else:
            print(file1)
    file2 = open("/home/felix/PycharmProjects/pythonProject/c_rest_of_shuffled_files/neue_shuffled_file.shuffled", "w")
    for line in shuffle_break:
        file2.write(line)
    file2.close()

    for j in range(20, 40):
        filename = "sentence_list{}.txt".format(j + 1)
        file3 = open("/home/felix/PycharmProjects/pythonProject/c_text_files/{}".format(filename), "w")
        for i in range(1000000):
            element = longlist.pop()
            file3.write(element)
        file3.close()


# spielt keine Rolle für das Erstellen der Co-Occurrence-Matrix
def change_file_name():
    neu_dir_path = "/home/felix/PycharmProjects/pythonProject/c_neu_tester"
    old_dir_path = "/home/felix/Dokumente/a_parsed_conll"
    for i in range(33, 1281):
        addpath = "/{}".format(i)
        old_dir_path_hier = old_dir_path + addpath
        filenames = os.listdir(old_dir_path_hier)
        for filename in filenames:
            if ".txt" not in filename:
                old_file_name = old_dir_path_hier + "/" + filename
                new_file_name = neu_dir_path + "/{}.conll".format(i)
                os.rename(old_file_name, new_file_name)
