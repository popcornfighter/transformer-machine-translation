import contextchains


def get_list_of_sentences(file):
    all_sentences = []
    # hier sind die Sätze noch im conll Format, also jedes Wort hat 12 Felder. Zunächst werden alle Satzzeichen entfernt
    # und dann wird ein Satz auf eine Liste von Wörtern reduziert.
    sentences = contextchains.get_sentences(file)
    for sentence in sentences:
        word_list = contextchains.get_word_list(sentence)
        word_list = contextchains.remove_dots(word_list)
        # reduziert auf Liste von Wörtern also zum Beispiel ein Wort könnte gleich "Haus" sein.
        this_sentence = get_sentence(word_list)
        all_sentences.append(this_sentence)
    return all_sentences


def get_sentence(word_list):
    sentence = []
    for word in word_list:
        sentence.append(word[1])
    return sentence


# hier werden die Sätze auf die Indizes im Wortschatz reduziert
def get_reduced_sentences(sentences, token_to_index: dict):
    list_of_sentences: list = sentences
    list_of_reduced_sentences = []
    list_len = 0
    while len(list_of_sentences) > 0:
        sentence = list_of_sentences.pop()
        # Ein Index > |Wortschatz| wird vor jede Sequenz von Indizes(= Satz) gesetzt um die Sätze beim
        # co-oc-count unterscheiden zu können
        reduced_sentence = [len(token_to_index) + 10]
        list_len = list_len + 1
        for word in sentence:
            if word in token_to_index:
                word_index = token_to_index[word]
                reduced_sentence.append(word_index)
                list_len = list_len + 1
        list_of_reduced_sentences.append(reduced_sentence)
    return list_of_reduced_sentences, list_len
