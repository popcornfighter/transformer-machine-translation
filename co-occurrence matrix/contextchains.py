import helper


# gets a list of all sentences in conll file, each sentence is a list of words, each word is the
# affiliated line in the conll file, like "2	ältere	alt	ADJ	ADJA	Degree=Cmp|Number=Plur	3	amod	_	_"
def get_sentences(file):
    file.seek(0)
    lines = file.readlines()
    these_sentences = []
    new_sentence = []
    for line in lines:
        if line.strip():
            new_sentence.append(line)
        else:
            these_sentences.append(new_sentence.copy())
            new_sentence = []
    if new_sentence:
        these_sentences.append(new_sentence.copy())
    return these_sentences


# gives the list of words in a sentence, each word is itself a list with 5 entries:
# [index in sentence, word, word stem, relation in the sentence, head of the relation]
def get_word_list(sentence):
    this_word_list = []
    for this_word in sentence:
        word_stuff = this_word.split()
        temp_list = [word_stuff[0], word_stuff[1], word_stuff[2], word_stuff[len(word_stuff) - 3],
                     word_stuff[len(word_stuff) - 4]]
        this_word_list.append(temp_list.copy())
        temp_list.clear()
    return this_word_list


# removes all symbols from word list, alle Satzzeichen haben den POS punct
def remove_dots(this_list: []):
    new_list = []
    for element in this_list:
        if element[3] != "punct":
            new_list.append(element)
    return new_list


def get_word_at_index_x(wordlist, x):
    returnword = []
    for word in wordlist:
        if word[0] == x:
            returnword = word
            break
    return returnword


def checkindexinwordlist(wordlist, headindex):
    for word in wordlist:
        wordindex = word[0]
        if wordindex == headindex:
            return True
    return False


def check_head_index(wordlist, headindex):
    if headindex == "0":
        return False
    elif not checkindexinwordlist(wordlist, headindex):
        return False
    else:
        return True


# creates context chain from wordlist of sentence
# danach ist ein Wort keine Liste mehr sondern nur noch ein Wort also zBsp "Haus"
# a context chain is a list with a head as the first element followed by all its dependents
def get_context_chain_list(word_list):
    # word_list is a sentence which is a list of words which is a list of things about the word
    # 1 word: [index in sentence, word, word stem, relation in the sentence, head of the relation]
    context_chain_list = []
    word_list_copy = word_list.copy()
    while len(word_list_copy) > 0:
        context_chain = []
        checkword = word_list_copy.pop()
        head_index_of_checkword = checkword[4]
        # wenn head nicht root und head in wordlist also kein Satzzeichen
        if check_head_index(word_list, head_index_of_checkword):
            head = get_word_at_index_x(word_list, head_index_of_checkword)
            context_chain.append(head)
            # [index in sentence, word, word stem, relation in the sentence, head of the relation]
            for it_word in word_list:
                # checks if the checkword and the it_word relate to the same head and then appends to contextchain
                if it_word[4] == head_index_of_checkword:
                    context_chain.append(it_word)
            context_chain_list.append(context_chain)
    # removes duplicates
    head_list = []
    new_context_chain_list = []
    for context_chain_t in context_chain_list:
        head_of_context_chain = context_chain_t[0]
        if head_of_context_chain not in head_list:
            head_list.append(head_of_context_chain)
            new_context_chain_list.append(context_chain_t)
    context_chain_list = new_context_chain_list.copy()
    # edits context_chain_list
    new_context_chain_list = []
    for context_chain_t in context_chain_list:
        new_context_chain = []
        # wort wird auf das Wort selbst reduziert
        # it wort: # [index in sentence, word, word stem, relation in the sentence, head of the relation]
        # also nur der zweite Eintrag dieser Liste bleibt für das Wort erhalten
        for it_word in context_chain_t:
            word = it_word[1]
            new_context_chain.append(word)
        # nur contextchains > 1 werden behalten
        if len(new_context_chain) > 1:
            new_context_chain_list.append(new_context_chain)
    return new_context_chain_list


# erstellt die Contextchains auf basis der Wortlisten in der File
def get_all_context_chains(list_of_wordlists: list):
    all_context_chains = []
    wordlists = []
    # iteriert über Liste von Sätzen bzw Wordlists
    while len(list_of_wordlists) > 0:
        word_list = list_of_wordlists.pop()
        context_chain_list = get_context_chain_list(word_list)
        check = False
        # TODO
        #  entfernt alle Kontextfenster von allen Wordlists, die mindestens ein Kontextfenster > 10 generieren.
        #  Test hat ergeben, dass es sich bei den falschen Word_Lists um 0,2% aller Wordlists handelt.
        #  Ich werde sie also entfernen. Die Wordlists sind mein Corpus und damit relevanter als die Context-Chains.
        #  Deswegen muss ich den Anteil der Context-Chains nicht überprüfen, er wäre auch nicht wesentlich größer.
        for context_chain in context_chain_list:
            if len(context_chain) > 10 and check is False:
                check = True
        if check is False:
            # alle validen Contextchains werden in einer Liste aller Kontextchains gespeichert.
            all_context_chains.extend(context_chain_list)
            # alle validen Word_lists werden auch gespeichert und zurückgegeben
            wordlists.append(word_list)
    return all_context_chains, wordlists


# ließt zunächst alle Sätze aus der conll file in eine Liste ein. Ein Satz ist dann eine Liste von Worten ein Wort
# ein Wort ist eine Zeile in der Conll File
# Dann wird ein Wort in eine Liste von Eigenschaften umgeformt:
# [index in sentence, word, word stem, relation in the sentence, head of the relation]
# dann werden Satzzeichen entfernt.
# Ein Satz ist jetzt also eine Liste von Worten und ein Wort ist eine Liste mit 5 Einträgen
# anschließend werden alle Sätze bzw Wortlisten aus der File in einer Liste von Sätzen bzw. Wortlisten gespeichert
# und zurückgegeben
def get_list_of_word_lists(file):
    all_word_lists = []
    sentences = get_sentences(file)
    for sentence in sentences:
        # [index in sentence, word, word stem, relation in the sentence, head of the relation]
        word_list = get_word_list(sentence)
        word_list = remove_dots(word_list)
        all_word_lists.append(word_list)
    return all_word_lists


# context chain besteht jetzt nur noch aus indices in Abhängigkeit von token_index_dict
# alle leeren context_chains werden gelöscht
def reduce_context_chains(context_chains_in: list, token_index_dict_in: dict):
    new_context_chains = []
    list_len = 0
    while len(context_chains_in) > 0:
        context_chain = context_chains_in.pop()
        context_index_chain = []
        for it_word in context_chain:
            # überprüft ob it_word teil der 60.000 häufigsten Vokabeln ist, wenn ja wird sein Index hinzugefügt
            # Das Problem ist, wenn der Head wegfällt, das wurde nicht gelöst.
            # Sinnvoller wäre zu vermerken, dass der Head weggefallen ist, oder das Kontextfenster zu entfern
            # so wird falsch gewichtet
            if it_word in token_index_dict_in:
                it_word_index = token_index_dict_in[it_word]
                context_index_chain.append(it_word_index)
        # nur solche Fenster mit mehr als einem Eintrag werden behalten
        if len(context_index_chain) > 1:
            # vor jede Kontextchain wird ein Index gesetzt der größer Ist als der Wortschatz,
            # um die Kontextchains beim CO-OC-Count unterscheiden zu können
            new_context_index_chain = [len(token_index_dict_in) + 10]
            list_len = list_len + 1
            for context_index in context_index_chain:
                new_context_index_chain.append(context_index)
                list_len = list_len + 1
            new_context_chains.append(new_context_index_chain)
    return new_context_chains, list_len
