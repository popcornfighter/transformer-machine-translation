import h5py
import numpy as np
import helper


# führt im Grunde nur die verschiedenen h5py Datasets zu einem großen zusammen für das Training der Modelle
def combine_hdf5_files():
    file = h5py.File("/home/felix/PycharmProjects/pythonProject/c_cooccurrence_hdf5_total/cooccurrencetotal.hdf5", "w")
    dataset = file.create_dataset("cooccurrence", (0, 3), maxshape=(None, 3), chunks=(100000, 3))
    time1 = helper.current_milli_time()
    xi = 0
    for ibor in range(30):
        xi = xi + 1
        print(xi)
        file1 = h5py.File("/home/felix/PycharmProjects/pythonProject/c_coocurrence_par_hdf5/"
                          "cooccurrence{}.hdf5".format(ibor), "r")
        dataset1 = file1["cooccurrence"]
        add_len = dataset1.len()
        old_len = dataset.len()
        co_occcurr_dataset = np.zeros((add_len, 3))
        for i in range(add_len):
            liste = dataset1[i]
            co_occcurr_dataset[i] = (liste[0], liste[1], liste[2])
        dataset.resize(old_len + add_len, 0)
        dataset[old_len: old_len+add_len] = co_occcurr_dataset
        file1.close()
    print("Zeit: " + str((helper.current_milli_time()-time1)//1000))
    file.close()


def combine_hdf5_files_glove():
    file = h5py.File("/home/felix/PycharmProjects/pythonProject/c_glove_cooccurrence_hdf5_total/"
                     "glove_cooccurrencetotal.hdf5", "w")
    dataset = file.create_dataset("cooccurrence", (0, 3), maxshape=(None, 3), chunks=(100000, 3))
    time1 = helper.current_milli_time()
    xi = 0
    for iber in range(30):
        xi = xi + 1
        print(xi)
        xab = 0
        file1 = h5py.File("/home/felix/PycharmProjects/pythonProject/c_glove_cooccurrence_parts_hdf5/"
                          "cooccurrence{}.hdf5".format(iber), "r")
        dataset1 = file1["cooccurrence"]
        add_len = dataset1.len()
        old_len = dataset.len()
        xab = 0
        co_occcurr_dataset = np.zeros((add_len, 3))
        for i in range(add_len):
            liste = dataset1[i]
            co_occcurr_dataset[i] = (liste[0], liste[1], liste[2])
        xab = 0
        dataset.resize(old_len + add_len, 0)
        dataset[old_len: old_len+add_len] = co_occcurr_dataset
        file1.close()
    print("Zeit: " + str((helper.current_milli_time()-time1)//1000))
    file.close()
