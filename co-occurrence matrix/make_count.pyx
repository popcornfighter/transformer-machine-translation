import os
import cython
import h5py
import numpy as np
from libc.stdlib cimport malloc, free, strtol
from libc.stdio cimport FILE, fopen, fgets, fclose, fseek, SEEK_SET
import helper
cimport numpy as np
np.import_array()
# TODO alle prints löschen
# macht cooccurrence count ~schnell
# als erstes werden die contextchainlist von der file eingelesen und dann je file der count durchgeführt
# und dann in h5py gespeichert
@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
@cython.cdivision(True)
cpdef makecount(int first_index, int indizes_slice_len, int indizes_len, int filenumber):
    time1 = helper.current_milli_time()
    # darf auch wirklich nicht größer werden als das. Also nicht größer als 480.000.000 Bytes

    # macht count für das Dependency-Glove-Modell

    # die Listen von Contextchains waren alle als .txt gespeichert, da ich diese in reinem c auslesen konnte
    # und dann als array context_list_cy im c format gespeichert um schnell verarbeitbar zu sein.

    # combinations ist eine Liste mit dem betrachteten Slice an Indizes im Wortschatz * |Wortschatz|
    # wird mit nullen gefüllt und anschließend wird der first index in den Listen von Contextchains
    # gesucht, wenn er gefunden wird und zum Beispiel index 10 im Kontext auftritt wird der 10. Eintrag von
    # Combinations also combinations[10] angepasst. Nachdem alle Kontexte von first_index in Combinations eingetragen
    # sind, wird in den Listen von Contextchains nach first_index+1 gesucht. Tritt Index 10 im Kontext von first_index+1
    # auf wird combinations[60000+10] angepasst. Das wird für alle Indizes im betrachteten Slice durchgeführt.
    # Danach wird combinations als Numpyarray (i,j,count) gespeichert in h5py-Format gespeichert.
    # Später wurden die erstellten hp5y-files zusammengeführt.
    # Der Count für das Glove-Modell funktioniert ähnlich, bis auf die Gewichtung
    # und es handelt sich dort nicht um Kontextfenster, sondern Sätze

    cdef float *combinations = <float *> malloc(indizes_slice_len*indizes_len*sizeof(float))
    cdef int i
    for i in range(indizes_slice_len*indizes_len):
        combinations[i] = 0
    cdef char *filepath
    cdef char *r = "r"
    cdef FILE *file
    cdef char *line = <char *> malloc(30*sizeof(char))
    cdef int *context_list_cy
    cdef int xo
    cdef char * newline = "\n"
    cdef int number
    cdef int context_list_len
    cdef char *ptr
    cdef int context_index
    cdef int j
    cdef int k
    cdef int throwaway = 0
    cdef int focuswordhead
    cdef int contextwordhead
    cdef float one = 1
    cdef float two = 2
    cdef float three = 3
    directorypath = "/home/felix/PycharmProjects/pythonProject/c_reduced_and_prepended_context_lists"
    file_list = os.listdir(directorypath)
    for filename in file_list:
        mystring = directorypath + "/" + filename
        stringtobytes = bytes(mystring, 'utf-8')
        filepath = stringtobytes
        file = fopen(filepath, r)
        fseek(file, 0, SEEK_SET)
        xo = 0
        while fgets(line, 14, file):
            if xo == 0:
                context_list_len = strtol(line, &ptr, 10)
                context_list_cy = <int *> malloc(context_list_len * sizeof(int))
                xo = xo + 1
            else:
                number = strtol(line, &ptr, 10)
                context_list_cy[xo-1] = number
                xo = xo + 1
        fclose(file)
        for i in range(first_index, first_index+indizes_slice_len):
            for j in range(context_list_len):
                # head ist immer links von allen dependents
                # schaut of fokuswort head ist, wenn ja werden alle dependents mit gewichtung 1/2 gezählt
                # wenn nein wird links vom fokuswort nach kontext gesucht, der head identifiziert und mit gewichtung 1
                # gezählt, alle weiteren dependents werden mit Gewichtung 1/3 gezählt
                # dann wird rechts vom fokuswort nach kontext gesucht und alle mit Gewichtung 1/3 gezählt
                if context_list_cy[j] == i:
                    focuswordhead = 0
                    if (j - 1) >= 0:
                        if context_list_cy[j-1] >= indizes_len:
                            focuswordhead = 1
                    if focuswordhead == 1:
                        k = 1
                        while k < 12:
                            if (j + k) <= (context_list_len - 1):
                                context_index = context_list_cy[j+k]
                                if context_index >= indizes_len:
                                    k = 14
                                else:
                                    combinations[(i - first_index) * indizes_len + context_index] += one/two
                                    k = k + 1
                            else:
                                k = 14
                    else:
                        k = 1
                        while k < 12:
                            if (j - k) >= 0:
                                context_index = context_list_cy[j-k]
                                if context_index >= indizes_len:
                                    k = 14
                                else:
                                    contextwordhead = 0
                                    if (j - k - 1) >= 0:
                                        if context_list_cy[j-k-1] >= indizes_len:
                                            contextwordhead = 1
                                    if contextwordhead == 1:
                                        combinations[(i - first_index) * indizes_len + context_index] += 1
                                        k = 14
                                    else:
                                        combinations[(i - first_index) * indizes_len + context_index] += one/three
                                        k = k + 1
                            else:
                                k = 14
                        k = 1
                        while k < 12:
                            if (j + k) <= (context_list_len - 1):
                                context_index = context_list_cy[j+k]
                                if context_index >= indizes_len:
                                    k = 14
                                else:
                                    combinations[(i - first_index) * indizes_len + context_index] += one/three
                                    k = k + 1
                            else:
                                k = 14
        free(context_list_cy)
    # combinations ist fertig für den betrachteten Teil des Wortschatzes, nun wird es als h5py dataset gespeichert.
    cdef int datasetlen = 0
    for i in range(indizes_slice_len*indizes_len):
        if combinations[i] != 0:
            datasetlen = datasetlen + 1
    co_occcurr_dataset = np.zeros([datasetlen, 3], dtype=float)
    cdef int index
    cdef int datasetindex = 0
    for index in range(indizes_slice_len):
        for j in range(indizes_len):
            if combinations[index*indizes_len+j] != 0:
                co_occcurr_dataset[datasetindex] = (first_index + index, j, combinations[index*indizes_len+j])
                datasetindex = datasetindex + 1
    free(combinations)
    a = first_index // 2000 # es waren 30 slices des Wortschatzes mit je 2000 Einträgen
    # dabei sollte es auch belassen werden, sonst funktioniert die Zeile nicht
    pathtofile = "/home/felix/PycharmProjects/pythonProject/c_coocurrence_par_hdf5/cooccurrence{}.hdf5".format(a)
    h5pyfile = h5py.File(os.path.join(pathtofile), "w")
    dataset = h5pyfile.create_dataset("cooccurrence", (datasetlen, 3), maxshape=(None, 3), chunks=(100000, 3))
    dataset[0: dataset.len()] = co_occcurr_dataset
    h5pyfile.close()
    print("Gesamtzeit: " + str(helper.current_milli_time()-time1))


@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
@cython.cdivision(True)
cpdef makecount_glove(int firstindex, int indizes_slices_len, int indizes_len, int filenumber):
    time1 = helper.current_milli_time()
    cdef float* combinations = <float *> malloc(indizes_slices_len*indizes_len*sizeof(float))
    cdef int i
    for i in range(indizes_len*indizes_slices_len):
        combinations[i] = 0
    cdef char* filepath
    cdef char *r = "r"
    cdef FILE *file
    cdef char *line = <char *> malloc(30*sizeof(char))
    cdef int *sentencelist_cy
    cdef int xo
    cdef char * newline = "\n"
    cdef int number
    cdef int sentencelistlen
    cdef char *ptr
    cdef int contextindex
    cdef int j
    cdef int k
    cdef float divider
    cdef float one = 1
    cdef int throwaway = 0
    directorypath = "/home/felix/PycharmProjects/pythonProject/c_reduced_and_prepended_sentences"
    filelist = os.listdir(directorypath)
    for filename in filelist:
        mystring = directorypath + "/" + filename
        stringtobytes = bytes(mystring, 'utf-8')
        filepath = stringtobytes
        file = fopen(filepath, r)
        fseek(file, 0, SEEK_SET)
        xo = 0
        while fgets(line, 14, file):
            if xo == 0:
                sentencelistlen = strtol(line, &ptr, 10)
                sentencelist_cy = <int *> malloc(sentencelistlen * sizeof(int))
                xo = xo + 1
            else:
                number = strtol(line, &ptr, 10)
                sentencelist_cy[xo - 1] = number
                xo = xo + 1
        fclose(file)
        for i in range(firstindex, firstindex + indizes_slices_len):
            for j in range(sentencelistlen):
                if sentencelist_cy[j] == i:
                    k = 1
                    # geht maximal 10 Schritte nach links bzw. rechts. Stoppt, wenn es die Bounds des Satzes
                    # verlässt, passt alle Einträge in combinations mit gefundenen Kontextwörtern an
                    while k < 11:
                        if (j - k) >= 0:
                            contextindex = sentencelist_cy[j-k]
                            if contextindex >= indizes_len:
                                k = 12
                            else:
                                divider = k
                                combinations[(i - firstindex) * indizes_len + contextindex] += one/divider
                                k = k + 1
                        else:
                            k = 12
                    k = 1
                    while k < 11:
                        if (j + k) <= (sentencelistlen - 1):
                            contextindex = sentencelist_cy[j+k]
                            if contextindex >= indizes_len:
                                k = 12
                            else:
                                divider = k
                                combinations[(i - firstindex) * indizes_len + contextindex] += one/divider
                                k = k + 1
                        else:
                            k = 12
        free(sentencelist_cy)
    time2 = helper.current_milli_time()
    print("long_time: " + str(time2-time1))
    cdef int datasetlen = 0
    for i in range(indizes_slices_len*indizes_len):
        if combinations[i] != 0:
            datasetlen = datasetlen + 1
    co_occurr_dataset = np.zeros([datasetlen, 3], dtype=float)
    cdef int index
    cdef int datasetindex = 0
    for index in range(indizes_slices_len):
        for j in range(indizes_len):
            if combinations[index*indizes_len+j] != 0:
                co_occurr_dataset[datasetindex] = (firstindex + index, j, combinations[index*indizes_len+j])
                datasetindex = datasetindex + 1
    free(combinations)
    a = firstindex // 2000
    pathtofile = "/home/felix/PycharmProjects/pythonProject/c_glove_cooccurrence_parts_hdf5/cooccurrence{}.hdf5".format(a)
    h5pyfile = h5py.File(os.path.join(pathtofile), "w")
    dataset = h5pyfile.create_dataset("cooccurrence", (datasetlen, 3), maxshape=(None, 3), chunks=(100000, 3))
    dataset[0: dataset.len()] = co_occurr_dataset
    h5pyfile.close()
    print("short time: " + str(helper.current_milli_time()-time2))
    print("Gesamtzeit: " + str(helper.current_milli_time()-time1))

cpdef test():
    cdef float a = 3
    cdef float b = 1
    cdef float c = 4
    cdef float x = 0
    x += b/a
    x += b/c
    print(x)