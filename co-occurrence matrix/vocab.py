import random


class Vocab:
    def __init__(self):
        self.token_to_index = {}
        self.index_to_token = {}
        self.token_to_stems = {}
        self.token_count = {}
        self.token_to_stems_after_sort = {}

    def add_token_stem(self, token, stem):
        value = self.token_to_stems.get(token)
        if value is not None:
            stemlist = value
            if stem not in stemlist:
                stemlist.append(stem)
            self.token_to_stems[token] = stemlist
        else:
            self.token_to_stems[token] = [stem]

    def add_token_count(self, token):
        value = self.token_count.get(token)
        if value is not None:
            self.token_count[token] = value + 1
        else:
            self.token_count[token] = 1

    def get_vocab_with_count_greater_k(self, k):
        tokens_after_check = []
        for token in self.token_to_stems:
            new_stems = []
            stemlist = self.token_to_stems[token]
            for stem in stemlist:
                stem_count = self.token_count[stem]
                if stem_count >= k:
                    new_stems.append(stem)
            self.token_to_stems_after_sort[token] = new_stems
            if self.token_count[token] >= k:
                tokens_after_check.append(token)
        index = 0
        # TODO shuffle tokens after check
        random.shuffle(tokens_after_check)
        for token in tokens_after_check:
            self.token_to_index[token] = index
            self.index_to_token[index] = token
            index = index + 1

    # TODO: token gesamt sind die Anzahl der Tokens in self.token_count
    def get_all_k_most_frequent_vocabs(self, k):
        tokenlist = []
        for token in self.token_count:
            tokenlist.append(token)
        # sortiert alle Tokens nach Häufigkeit
        tokenlist.sort(key=lambda s: self.token_count[s], reverse=True)
        tokenlistlen = len(tokenlist)
        reduced_tokenlist = []
        # behält die ersten k Einträhe
        for i in range(k):
            token = tokenlist[i]
            reduced_tokenlist.append(token)
        tokenlist.clear()
        # das mit den Stems wird nicht weiter verwendet
        for token in reduced_tokenlist:
            value = self.token_to_stems.get(token)
            if value is not None:
                old_stems = self.token_to_stems[token]
                new_stems = []
                for stem in old_stems:
                    if stem in reduced_tokenlist:
                        new_stems.append(stem)
                self.token_to_stems_after_sort[token] = new_stems
        # speichert die Gesamtanzahl der verschiedenen Wörter
        open_file = open("/home/felix/PycharmProjects/pythonProject/c_vocab/vocabinfo.txt", "w")
        open_file.write("Gesamtanzahl an Wörtern: {}\n".format(tokenlistlen))
        lasttoken = reduced_tokenlist[k-1]
        mincount = self.token_count[lasttoken]
        # speichert die minimale Häufigkeit aller Wörter in der Vocabulary
        open_file.write("Minimale Häufigkeit der Wörter in der Vocabulary: {}\n".format(mincount))
        open_file.close()
        random.shuffle(reduced_tokenlist)
        index = 0
        # weist jedem Index einen Token zu. Von den k häufigsten Tokens.
        for token in reduced_tokenlist:
            self.token_to_index[token] = index
            self.index_to_token[index] = token
            index = index + 1

    def get_all_k_most_frequent_vocab_for_glove(self, k):
        tokenlist = []
        for token in self.token_count:
            tokenlist.append(token)
        tokenlist.sort(key=lambda s: self.token_count[s], reverse=True)
        tokenlistlen = len(tokenlist)
        reduced_tokenlist = []
        for i in range(k):
            token = tokenlist[i]
            reduced_tokenlist.append(token)
        tokenlist.clear()
        open_file = open("/home/felix/PycharmProjects/pythonProject/c_glove_vocab/vocabinfo.txt", "w")
        open_file.write("Gesamtanzahl an Wörtern: {}\n".format(tokenlistlen))
        lasttoken = reduced_tokenlist[k - 1]
        mincount = self.token_count[lasttoken]
        x = 0
        open_file.write("Minimale Häufigkeit der Wörter in der Vocabulary: {}\n".format(mincount))
        open_file.close()
        random.shuffle(reduced_tokenlist)
        index = 0
        for token in reduced_tokenlist:
            self.token_to_index[token] = index
            self.index_to_token[index] = token
            index = index + 1

