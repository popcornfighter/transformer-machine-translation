import helper
import vocab
import concurrent.futures
import make_count


# führt den co-oc-count in parallel aus für das Dependency GloveModell
def runcoocinparallel():
    index_slices_list, number_list, token_len_list = helper.get_parallel_parameters()
    time1 = helper.current_milli_time()
    with concurrent.futures.ProcessPoolExecutor() as executor:
        executor.map(call_cython_cooc, index_slices_list, token_len_list, number_list)
    print("Gesamtzeit in Sekunden: " + str((helper.current_milli_time() - time1) // 1000))
    print("Zeit für einen Corpus mit ca. 630 Millionen Wörtern und Vocab = 60.000 in Stunden: " +
          str(((helper.current_milli_time() - time1) // 1000) // 3600))


def call_cython_cooc(index_list, token_len, number):
    firstindex = index_list[0]
    index_list_len = len(index_list)
    # number wird nicht mehr verwendet.
    make_count.makecount(firstindex, index_list_len, token_len, number)


# führt den co-oc-count in parallel aus für das GloveModell
def runglovecoocinparallel():
    index_slices_list, number_list, token_len_list = helper.get_glove_parallel_parameters()
    time1 = helper.current_milli_time()
    with concurrent.futures.ProcessPoolExecutor() as executor:
        executor.map(call_cython_cooc_glove, index_slices_list, token_len_list, number_list)
    print("Gesamtzeit in Sekunden: " + str((helper.current_milli_time() - time1) // 1000))
    print("Zeit für einen Korpus mit 615.287.176 Wörtern und den Top 60.000 Vokabeln in Stunden: " +
          str(((helper.current_milli_time() - time1) // 1000) // 3600))


def call_cython_cooc_glove(index_list, token_len, number):
    firstindex = index_list[0]
    index_list_len = len(index_list)
    # number wird nicht mehr verwendet
    make_count.makecount_glove(firstindex, index_list_len, token_len, number)


# setzt die Contextchains für das Dependency Glove Modell
def set_contextchain_and_vocab():
    time1 = helper.current_milli_time()
    # erstellt eine Instanz der Klasse Vocab um die tokens zu indizieren und zu zählen
    vocabulary = vocab.Vocab()
    directorypath = "/home/felix/PycharmProjects/pythonProject/c_corupus_conll_files"
    # erklärung in helper
    helper.set_vocab_and_context_chains(directorypath, vocabulary)
    time2 = helper.current_milli_time()
    print("first time: " + str(time2 - time1))
    # and token count greater l
    # vocabulary.get_vocab_with_count_greater_k(20)
    # stellt liste mit den 60.000 häufigsten Tokens auf
    vocabulary.get_all_k_most_frequent_vocabs(60000)
    time3 = helper.current_milli_time()
    print("second time: " + str(time3 - time2))
    # weißt auf die Liste mit den Contextchainlisten, die aus den Corpus-Conll-Files erstellt wurden
    directorypath = "/home/felix/PycharmProjects/pythonProject/c_contextchainlists"
    helper.set_reduced_context_chains(directorypath, vocabulary)
    time4 = helper.current_milli_time()
    print("thrid time: " + str(time4 - time3))
    directorypath = "/home/felix/PycharmProjects/pythonProject/c_reduced_context_lists"
    # fügt allen Files im Directorypath die Anzahl der Zeilen hinzu, das braucht die Funktion
    # die den CO-OC-COUNT macht. Speichert diese in neuer Directory
    helper.prepend_line_number(directorypath)
    time5 = helper.current_milli_time()
    print("fourth time: " + str(time5 - time4))
    # speichert die token_to_index dictionary und andere Vocab daten
    directorypath = "/home/felix/PycharmProjects/pythonProject/c_vocab/"
    helper.save_vocab_entries(directorypath, vocabulary)
    time6 = helper.current_milli_time()
    print("fifth time: " + str(time6 - time5))
    print("token_to_index_len: " + str(len(vocabulary.token_to_index)))
    print("total time: " + str(time6 - time1))


# setzt die Liste mit reduzierten Vocab entries
def set_contextwindows_and_vocab():
    time1 = helper.current_milli_time()
    vocabulary = vocab.Vocab()
    # iteriert über alle Corpus conll files im angegeben directory path
    directorypath = "/home/felix/PycharmProjects/pythonProject/c_corupus_conll_files"
    helper.set_vocab_and_contextwindows(directorypath, vocabulary)
    print(len(vocabulary.token_count))
    time2 = helper.current_milli_time()
    print("first time: " + str(time2 - time1))
    # behält nur die 60.000 häufigsten Tokens, erstellt die Token-Index Dictionaries
    vocabulary.get_all_k_most_frequent_vocab_for_glove(60000)
    time3 = helper.current_milli_time()
    print("second time: " + str(time3 - time2))
    # Reduziert die Sätze auf Indizes im Wortschatz
    directorypath = "/home/felix/PycharmProjects/pythonProject/c_glove_sentences"
    helper.set_reduced_sentences(directorypath, vocabulary)
    time4 = helper.current_milli_time()
    print("third time: " + str(time4 - time3))
    # Fügt eine Zeilennummer an den Anfang der Files hinzu
    directorypath = "/home/felix/PycharmProjects/pythonProject/c_reduced_sentences"
    helper.prepend_glove_line_number(directorypath)
    time5 = helper.current_milli_time()
    print("fourth time: " + str(time5 - time4))
    # speichert die Vocab-Daten für das Glove Modell
    directorypath = "/home/felix/PycharmProjects/pythonProject/c_glove_vocab/"
    helper.save_vocab_entries(directorypath, vocabulary)
    time6 = helper.current_milli_time()
    print("fifth time: " + str(time6 - time5))
    print("token_to_index_len: " + str(len(vocabulary.token_to_index)))
    print("total time: " + str(time6 - time1))
