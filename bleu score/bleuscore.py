import random
# two references for one document
from nltk.translate.bleu_score import corpus_bleu
import pickle

file1 = open("bleustuff.pkl", "rb")
di = pickle.load(file1)
src = di["src"]
ref = di["ref"]
glove_translation = di["glove_translation_5"]
d_glove_translation = di["glove_translation_7"]


index_list: list = []
for i in range(2998):
    index_list.append(i)


glove_score_list = []
d_glove_score_list = []
x = 15*3.7948475
cc = 1000
for k in range(cc):
    if k % 100 == 0:
        print(k)
    random.seed(x)
    x = x+1000/3.46868
    new_index_list = random.sample(index_list, 1000)
    reference1: list = []
    glove_candidate: list = []
    d_glove_candidate: list = []
    for t in range(1000):
        reference1.append([ref[new_index_list[t]].split()])
        glove_candidate.append(glove_translation[new_index_list[t]].split())
        d_glove_candidate.append(d_glove_translation[new_index_list[t]].split())
    glove_score = corpus_bleu(reference1, glove_candidate, weights=(1/4, 1/4, 1/4, 1/4))
    glove_score_list.append(glove_score)
    d_glove_score = corpus_bleu(reference1, d_glove_candidate, weights=(1/4, 1/4, 1/4, 1/4))
    d_glove_score_list.append(d_glove_score)

glove_total = 0
glove_score_list.sort()
for score in glove_score_list[20:980]:
    glove_total = glove_total + score
glove_avg = glove_total/cc

d_glove_total = 0
d_glove_score_list.sort()
for score in d_glove_score_list[20:980]:
    d_glove_total = d_glove_total + score
d_glove_avg = d_glove_total/cc
print("Glove-Score: ", glove_avg)
print("Glove-Score: ", d_glove_avg)
