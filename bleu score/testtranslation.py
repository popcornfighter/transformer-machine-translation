from datetime import datetime
import logging
import time
import numpy as np
import tensorflow_datasets as tfds
import tensorflow as tf
import pickle
# Import tf_text to load the ops used by the tokenizer saved model
import tensorflow_text  # pylint: disable=unused-import
from tensorflow.python.ops.gen_dataset_ops import TakeDataset
import time


def current_milli_time():
    return round(time.time() * 1000)


file1 = open("neu_finaltest.de", "r")
german_lines = file1.readlines()
file2 = open("neu_finaltest.en", "r")
eng_lines = file2.readlines()
file1.close()
file2.close()

german_sentences = []
for line in german_lines:
    st: str = line
    nst = st.replace("\n", "")
    german_sentences.append(nst)

eng_sentences = []
for line in eng_lines:
    st: str = line
    nst = st.replace("\n", "")
    eng_sentences.append(nst)


logging.getLogger('tensorflow').setLevel(logging.ERROR)  # suppress warnings
load_options = tf.saved_model.LoadOptions(experimental_io_device='/job:localhost')
# d_glove_reloaded = tf.saved_model.load("d_glove_translator", options=load_options)
glove_reloaded = tf.saved_model.load("glove_translator", options=load_options)

glove_translations = []
d_glove_translations = []

time1 = current_milli_time()

for t in range(len(german_sentences)):
    # d_glove_translations.append(d_glove_reloaded(german_sentences[t]).numpy().decode("utf-8"))
    glove_translations.append(glove_reloaded(german_sentences[t]).numpy().decode("utf-8"))

print("time: " + str(current_milli_time()-time1))

file3 = open("bleustuff.pkl", "rb")
di = pickle.load(file3)
file3.close()

di["glove_translation_7"] = glove_translations


for key in di:
    print(key)

file4 = open("bleustuff.pkl", "wb")
pickle.dump(di, file4)
file4.close()
