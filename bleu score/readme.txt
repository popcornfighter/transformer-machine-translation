Das verwendete Testset befindet sich im Ordner Testdaten. Die Originalen Dateien und auch die extrahierten und dann mit den Mosetools bearbeiteten files.
Für den Bleu-Test wurde neu_finaltest.de/en verwendet.

Die requirements sind angegeben.

Zunächst wurde das komplette Testset Satz für Satz von Deutsch nach Englisch übersetzt und die Sätze in einer Liste gespeichert.
Der Code dafür ist in testtranslation.py zu finden.

Die englischen Referenzen wurden auch in einr Liste gespeichert.
Zusammen ist das in bleustuff.pkl gespeichert.
Dort wurden (d_)glove_translations_5 und 7 verwendet für die Arbeit.

Die Qualität der Übersetzungen wurde in bleuscore.py geprüft. Dabei wurden die Referenzen und Übersetzungen aus bleustuff.pkl geladen und anhand von 1000
zufällig ausgewählten Sätzen verglichen. Das wurde 1000 mal wiederholt und die 20 besten und schlechtesten Ergebnisse fallen gelassen.
Der Durchschnittswert wurde in der Arbeit angegeben.
