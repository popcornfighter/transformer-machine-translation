Die Requirements sind requirements.txt von pycharm generiert.

Also um die Co-Occurrence Matrizen zu trainieren muss main gestartet werden
Die Filepaths in den set config Funktionen in step_two.py müssen manuell angepasst werden
Alles ist getrennt nach glove_modell und dependency_glove modell
Auch die vocab_daten und die co-occurrence-matrix

Das Training ist in train.py implementiert. Es wird der dataloader aus hdf5_dataloader.py verwendet.
Die train_glove Funktion lädt die passenden Hyperparameter und die Co-Occurrence-Matrix aus der übergeben config file.
Das GloVe Modell ist in glove.py zu finden.
Nach dem durchlauf des Trainings werden die Losses geprinted.


Anschließend wurde die jeweilige Embeddingmatrix aus dem trainierten Modell extrahiert.
Das Skript dafür finde ich nicht mehr, es wurde allerdings mit
from gensim.models.keyedvectors import KeyedVectors
gemacht. Die indizes zu den jeweiligen Tokens sind in index_to_token.pkl bzw. token_to_index.pkl gespeichert.

weiterhin wurden für beide Embeddingmatrizen zusätzlich Satzzeichen und die Reserved Tokens
durch zufällig generierte Vektoren hinzugefügt. Die zufälligen Vektoren sind für beide Matrizen identisch.
Der Code dafür ist in randomvectors.py. Der finale Wortschatz ist also der Wortschatz in token_to_index.pkl
plus die Liste in randomvectors.py an den Anfang gesetzt. Zu sehen in d_glove_de_vocab.txt (mit dependency information) bzw
d_glove_de_vocab.txt (ohne dependency information)
