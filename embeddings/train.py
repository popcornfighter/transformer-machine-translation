import argparse
import pickle
import time
from datetime import datetime
from pathlib import Path
import torch.optim
from matplotlib import pyplot as plt
from tqdm import tqdm
from glove import GloVe
from hdf5_dataloader import HDF5DataLoader
import yaml


def load_glove_config():
    config_filepath = Path(__file__).absolute().parents[1] / "pythonProject1/glove_config.yaml"
    with config_filepath.open() as f:
        config_dict = yaml.load(f, Loader=yaml.FullLoader)
    config = argparse.Namespace()
    for key, value in config_dict.items():
        setattr(config, key, value)
    return config


def load_dependency_glove_config():
    config_filepath = Path(__file__).absolute().parents[1] / "pythonProject1/dependency_glove_config.yaml"
    with config_filepath.open() as f:
        config_dict = yaml.load(f, Loader=yaml.FullLoader)
    config = argparse.Namespace()
    for key, value in config_dict.items():
        setattr(config, key, value)
    return config

# öffnet dataloader, der stellt die batches bereit, es wird der forwardpass durchgeführt und anschließend werden die Parameter angepasst.
def train_glove(config):
    dataloader = HDF5DataLoader(filepath=config.cooc_filepath, dataset_name="cooccurrence",
                                batch_size=config.batch_size, device=config.device)
    model = GloVe(vocab_size=config.vocab_size, embedding_size=config.embedding_size,
                  x_max=config.x_max, alpha=config.alpha)
    model.to(config.device)
    optimizer = torch.optim.Adagrad(model.parameters(), lr=config.learning_rate)
    with dataloader.open():
        model.train()
        losses = []
        for epoch in tqdm(range(config.num_epochs), position=0, leave=True):
            epoch_loss = 0
            for batch in tqdm(dataloader.iter_batches(), position=0, leave=True):
                loss = model(
                    batch[0][:, 0],
                    batch[0][:, 1],
                    batch[1]
                )
                epoch_loss += loss.detach().item()
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
            losses.append(epoch_loss)
            print("epoch done")
            print(f"Epoch {epoch}: loss = {epoch_loss}")
            torch.save(model.state_dict(), config.output_filepath)
        # TODO die Losses für den Verlauf der Losses über das Training
        for el in losses:
            print(el)
