import similarity
import train


# 32, 5, "cpu", 0.05, 100, 100, 0.75
def set_dependency_glove_config(batchsize: int, numepochs: int, device: str, learningrate: float, embeddingsize: int, xmax: int,
               alpha: float):
    vocabsize = get_dependency_glove_token_len()
    index_to_token_dict_filepath = "/home/felix/PycharmProjects/pythonProject1/c_dependency_glove_vocab/" \
                                   "index_to_token.pkl"
    coocfilepath = "/home/felix/PycharmProjects/pythonProject1/c_dependency_glove_cooccurrence_hdf5_total/" \
                   "cooccurrencetotal.hdf5"
    modeloutputfilepath = "/home/felix/PycharmProjects/pythonProject1/c_dependency_glove_mymodel/my_model.txt"
    configfile = open("/home/felix/PycharmProjects/pythonProject1/dependency_glove_config.yaml", "w")
    configfile.write("vocab_size: " + str(vocabsize) + "\n")
    configfile.write("index_token_dic_filepath: " + index_to_token_dict_filepath + "\n")
    configfile.write("cooc_filepath: " + coocfilepath + "\n")
    configfile.write("output_filepath: " + modeloutputfilepath + "\n")
    configfile.write("batch_size: " + str(batchsize) + "\n")
    configfile.write("num_epochs: " + str(numepochs) + "\n")
    configfile.write("device: " + device + "\n")
    configfile.write("learning_rate: " + str(learningrate) + "\n")
    configfile.write("embedding_size: " + str(embeddingsize) + "\n")
    configfile.write("x_max: " + str(xmax) + "\n")
    configfile.write("alpha: " + str(alpha))
    configfile.close()


# 32, 5, "cpu", 0.05, 100, 100, 0.75
def set_glove_config(batchsize: int, numepochs: int, device: str, learningrate: float, embeddingsize: int,
                                xmax: int, alpha: float):
    vocabsize = get_glove_token_len()
    index_to_token_dict_filepath = "/home/felix/PycharmProjects/pythonProject1/c_glove_vocab/index_to_token.pkl"
    coocfilepath = "/home/felix/PycharmProjects/pythonProject1/c_glove_cooccurrence_hdf5_total/" \
                   "glove_cooccurrencetotal.hdf5"
    modeloutputfilepath = "/home/felix/PycharmProjects/pythonProject1/c_glove_mymodel/my_model.txt"
    configfile = open("/home/felix/PycharmProjects/pythonProject1/glove_config.yaml", "w")
    configfile.write("vocab_size: " + str(vocabsize) + "\n")
    configfile.write("index_token_dic_filepath: " + index_to_token_dict_filepath + "\n")
    configfile.write("cooc_filepath: " + coocfilepath + "\n")
    configfile.write("output_filepath: " + modeloutputfilepath + "\n")
    configfile.write("batch_size: " + str(batchsize) + "\n")
    configfile.write("num_epochs: " + str(numepochs) + "\n")
    configfile.write("device: " + device + "\n")
    configfile.write("learning_rate: " + str(learningrate) + "\n")
    configfile.write("embedding_size: " + str(embeddingsize) + "\n")
    configfile.write("x_max: " + str(xmax) + "\n")
    configfile.write("alpha: " + str(alpha))
    configfile.close()


def get_glove_token_len():
    file = open("/home/felix/PycharmProjects/pythonProject1/c_glove_vocab/token_len.txt", "r")
    lines = file.readlines()
    a: int = int(lines[0])
    lines.clear()
    file.close()
    return a


def get_dependency_glove_token_len():
    file = open("/home/felix/PycharmProjects/pythonProject1/c_dependency_glove_vocab/token_len.txt", "r")
    lines = file.readlines()
    a: int = int(lines[0])
    lines.clear()
    file.close()
    return a


def get_glove_config():
    config = train.load_glove_config()
    return config


def get_dependency_glove_config():
    config = train.load_dependency_glove_config()
    return config


def do_dependency_glove_training():
    config = get_dependency_glove_config()
    train.train_glove(config)


def do_glove_training():
    config = get_glove_config()
    train.train_glove(config)
