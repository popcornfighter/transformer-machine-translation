import pathlib
import tensorflow_datasets as tfds
import tensorflow as tf
from tensorflow_text.tools.wordpiece_vocab import bert_vocab_from_dataset as bert_vocab
import time
import tensorflow_text as text
import re


def current_milli_time():
    return round(time.time() * 1000)


def write_vocab_file(filepath, vocab):
    with open(filepath, 'w') as f:
        for token in vocab:
            print(token, file=f)


def create_en_vocab_token_files():
    tf.get_logger().setLevel('ERROR')
    pwd = pathlib.Path.cwd()
    examples, metadata = tfds.load('my_dataset', with_info=True, as_supervised=True)
    train_examples = examples['train']
    train_en = train_examples.map(lambda de, en: en)
    bert_tokenizer_params = dict(lower_case=False)
    reserved_tokens = ["[PAD]", "[UNK]", "[START]", "[END]"]
    bert_vocab_args = dict(
        # The target vocabulary size
        vocab_size=60000,
        # Reserved tokens that must be included in the vocabulary
        reserved_tokens=reserved_tokens,
        # Arguments for `text.BertTokenizer`
        bert_tokenizer_params=bert_tokenizer_params,
        # Arguments for `wordpiece_vocab.wordpiece_tokenizer_learner_lib.learn`
        learn_params={},
    )
    time1 = current_milli_time()
    en_vocab = bert_vocab.bert_vocab_from_dataset(
        train_en.batch(1000).prefetch(2),
        **bert_vocab_args
    )
    time2 = current_milli_time()
    print((time2 - time1) // 1000)
    write_vocab_file('en_vocab.txt', en_vocab)


def add_start_end(ragged, start, end):
    count = ragged.bounding_shape()[0]
    starts = tf.fill([count, 1], start)
    ends = tf.fill([count, 1], end)
    return tf.concat([starts, ragged, ends], axis=1)


def cleanup_text(reserved_tokens, token_txt):
    # Drop the reserved tokens, except for "[UNK]".
    bad_tokens = [re.escape(tok) for tok in reserved_tokens if tok != "[UNK]"]
    bad_token_re = "|".join(bad_tokens)

    bad_cells = tf.strings.regex_full_match(token_txt, bad_token_re)
    result = tf.ragged.boolean_mask(token_txt, ~bad_cells)

    # Join them into strings.
    result = tf.strings.reduce_join(result, separator=' ', axis=-1)

    return result


def export_tokenizer(model_name_, de_vocab_file_name_):
    reserved_tokens = ["[PAD]", "[UNK]", "[START]", "[END]"]
    start = tf.argmax(tf.constant(reserved_tokens) == "[START]")
    end = tf.argmax(tf.constant(reserved_tokens) == "[END]")
    tokenizers = tf.Module()
    tokenizers.de = CustomTokenizer(reserved_tokens, de_vocab_file_name_, start, end)
    tokenizers.en = CustomTokenizer(reserved_tokens, 'en_vocab.txt', start, end)
    tf.saved_model.save(tokenizers, model_name_)


class CustomTokenizer(tf.Module):
    def __init__(self, reserved_tokens, vocab_path, start, end):
        self.tokenizer = text.BertTokenizer(vocab_path, lower_case=True)
        self._reserved_tokens = reserved_tokens
        self._vocab_path = tf.saved_model.Asset(vocab_path)
        self.start = start
        self.end = end
        vocab = pathlib.Path(vocab_path).read_text().splitlines()
        self.vocab = tf.Variable(vocab)
        # Create the signatures for export:
        # Include a tokenize signature for a batch of strings.
        self.tokenize.get_concrete_function(
            tf.TensorSpec(shape=[None], dtype=tf.string))

        # Include `detokenize` and `lookup` signatures for:
        #   * `Tensors` with shapes [tokens] and [batch, tokens]
        #   * `RaggedTensors` with shape [batch, tokens]
        self.detokenize.get_concrete_function(
            tf.TensorSpec(shape=[None, None], dtype=tf.int64))
        self.detokenize.get_concrete_function(
            tf.RaggedTensorSpec(shape=[None, None], dtype=tf.int64))

        self.lookup.get_concrete_function(
            tf.TensorSpec(shape=[None, None], dtype=tf.int64))
        self.lookup.get_concrete_function(
            tf.RaggedTensorSpec(shape=[None, None], dtype=tf.int64))

        # These `get_*` methods take no arguments
        self.get_vocab_size.get_concrete_function()
        self.get_vocab_path.get_concrete_function()
        self.get_reserved_tokens.get_concrete_function()

    @tf.function
    def tokenize(self, strings):
        enc = self.tokenizer.tokenize(strings)
        # Merge the `word` and `word-piece` axes.
        enc = enc.merge_dims(-2, -1)
        enc = add_start_end(enc, self.start, self.end)
        return enc

    @tf.function
    def detokenize(self, tokenized):
        words = self.tokenizer.detokenize(tokenized)
        return cleanup_text(self._reserved_tokens, words)

    @tf.function
    def lookup(self, token_ids):
        return tf.gather(self.vocab, token_ids)

    @tf.function
    def get_vocab_size(self):
        return tf.shape(self.vocab)[0]

    @tf.function
    def get_vocab_path(self):
        return self._vocab_path

    @tf.function
    def get_reserved_tokens(self):
        return tf.constant(self._reserved_tokens)


# TODO first run create_vocab dann export_tokenizer
create_en_vocab_token_files()
model_name = "d_glove_de_en_tokenize"
de_vocab_file_name = "d_glove_vocab.txt"
export_tokenizer(model_name, de_vocab_file_name)
